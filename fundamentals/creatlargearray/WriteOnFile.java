package creatlargearray;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class WriteOnFile {
    private PrintWriter outPutFile;
    private ArrayManagment arrayManegment;
    private int array[];

    public WriteOnFile(int array[])  {
       this.array = array;
       writeItemArrayOfFile();
    }

    public void writeItemArrayOfFile() {
        try {
            outPutFile = new PrintWriter("array.txt");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        for (int i =0; i< array.length;i++){
            outPutFile.println(array[i]) ;
        }
        outPutFile.close();
    }
}

