package creatlargearray;

import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;

public class ArrayManagment {
  private int num;
    private int numbers[];
    private double sumOfArray;
    private double avareg;


    public ArrayManagment(){
        num = inputItemFromUser();
        numbers = new int[num];
        fillArray(numbers);
        printItemOfArray(numbers);
        System.out.println("calculate sum of array =  "+getSumOfArray());
        System.out.println("calculate avareg of array =  "+getAvareg());
        getRepeatedNumbers(numbers,num);
        WriteOnFile writeOnFile = new WriteOnFile(numbers);

    }

    public int getNum() {
        return num;
    }

    public int[] getNumbers() {
        if (numbers== null)
            fillArray(numbers);
        return numbers;
    }


    // get size Of Array from users  0-0
    public  int inputItemFromUser(){
        Scanner scanner = new Scanner(System.in);
        boolean check = true;
        System.out.println("please enter the size of array note the size should grather than 10000");
        while (check){
            num = scanner.nextInt();
            if(checkNumber(num)){
                check = false;
                System.out.println("done");
            }
            else System.out.println("please enter the valid number");
        }
        return num;
    }

    // check size
    private  boolean checkNumber(int num) {
        return (num >= 10000)?true:false;
    }


    private  void fillArray(int[] numbers) {
        Random random = new Random();
        for (int a = 0; a < numbers.length; a++) {
            numbers[a] = random.nextInt(1000)+1;
        }
    }

    private  void calculateavaregOfArray(int[] numbers)
    {
        avareg =  calculateSumNumbersOfArray(numbers)/numbers.length;
    }

    private  double calculateSumNumbersOfArray(int[] numbers) {
        int count = 0;
        for(int i=0; i<numbers.length; i++) {
            sumOfArray = sumOfArray+numbers[i];
        }
        return sumOfArray;
    }

    public double getAvareg() {
        if (avareg == 0)
            calculateavaregOfArray(numbers);
        return avareg;
    }

    public double getSumOfArray() {
        if (sumOfArray == 0)
            calculateSumNumbersOfArray(numbers);
        return sumOfArray;
    }

    public void printItemOfArray(int numbers[]){
        for (int i =0; i<numbers.length;i++){
            System.out.println(numbers[i]);
        }
    }

    public void getRepeatedNumbers(int[] numbers, int n)
    {
        TreeMap<Integer, Integer> freq = new TreeMap<>();
        for (int i = 0; i < n; i++)
        {
            if (freq.containsKey(numbers[i]))
                freq.put(numbers[i], 1 + freq.get(numbers[i]));
            else
                freq.put(numbers[i], 1);
        }
        Pair x = new Pair();
        Pair y = new Pair();
        Pair z = new Pair();
        x.first = y.first = z.first = Integer.MIN_VALUE;
        for (Map.Entry val : freq.entrySet())
        {
            if (Integer.parseInt(String.valueOf(val.getValue())) > x.first)
            {
                z.first = y.first;
                z.second = y.second;
                y.first = x.first;
                y.second = x.second;
                x.first = Integer.parseInt(String.valueOf(val.getValue()));
                x.second = Integer.parseInt(String.valueOf(val.getKey()));
            }
            else if (Integer.parseInt(String.valueOf(val.getValue())) > y.first)
            {
                z.first = y.first;
                z.second = y.second;
                y.first = Integer.parseInt(String.valueOf(val.getValue()));
                y.second = Integer.parseInt(String.valueOf(val.getKey()));
            }
            else if (Integer.parseInt(String.valueOf(val.getValue())) > z.first)
            {
                z.first = Integer.parseInt(String.valueOf(val.getValue()));
                z.second = Integer.parseInt(String.valueOf(val.getKey()));
            }
        }
        System.out.println("Top three repeated elements are " + x.second + ", "+ y.second + ", " + z.second);
    }
}
class Pair{
    int first,second;

}
